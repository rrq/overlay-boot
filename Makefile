ETCDIR = $(DESTDIR)/etc
INITDIR = $(DESTDIR)/etc/init.d
MAN8DIR = $(DESTDIR)/usr/share/man/man8
SBINDIR = $(DESTDIR)/usr/sbin
VARLIBDIR = $(DESTDIR)/var/lib/overlay-boot

MAN8FILES = overlay-boot.8 overlay-go.8 overlay-stop.8 overlay-diskfile.8
MAN8FILES += overlay-share.8
VARLIBFILES = overlay-boot overlay-go overlay-stop functions reaper
VARLIBFILES += overlay-init overlay-postmount overlay-premount
VARLIBFILES += overlay-diskfile
VARLIBFILES += overlay-share
INITFILES = subhosts
ETCFILES = subhosts.conf

# The default is to build asm/reaper
REAPER = src/reaper
all: $(REAPER)

# Specific rule: $(REAPER) is ensured by "recursive make"
$(REAPER): 
	$(MAKE) -C $$(dirname $(REAPER)) reaper

# Specific rule: the reaper program originates from $(REAPER)
reaper: $(REAPER)
	mv $< $@

# Specific rule: the sysvinit init file
$(INITDIR)/subhosts: subhosts.sh | $(INITDIR)/
	cp $< $@

# Specific rule: the sysvinit init configuration file
$(ETCDIR)/subhosts.conf: subhosts.conf | $(ETCDIR)/
	cp $< $@

# Generic rule: any dependee directory needs to be created
%/:
	mkdir -p $@

# Generic rule: a local .8 file depends on the same .8.adoc file, if any
$(MAN8FILES): %: %.adoc
	a2x -d manpage -f manpage $^

# Generic rule: an installed MAN8FILE file depends on a local the
# same, and that the installation directory exists
$(addprefix $(MAN8DIR)/,$(MAN8FILES)): $(MAN8DIR)/%: % | $(MAN8DIR)/
	cp $< $@

# Generic rule: an installed VARLIBFILES file depends on a local the
# same, and that the installation directory exists
$(addprefix $(VARLIBDIR)/,$(VARLIBFILES)): $(VARLIBDIR)/%: % | $(VARLIBDIR)/
	cp -p $< $@

# Make target: to clean up this workspace
clean:
	rm -f reaper *.8
	make -C $$(dirname $(REAPER)) clean

.INTERMEDIATE: reaper $(MAN8FILES)

# Make target: enumerates that which should be installed
INSTALLTARGETS += $(addprefix $(MAN8DIR)/,$(MAN8FILES))
INSTALLTARGETS += $(addprefix $(VARLIBDIR)/,$(VARLIBFILES))
INSTALLTARGETS += $(addprefix $(INITDIR)/,$(INITFILES))
INSTALLTARGETS += $(addprefix $(ETCDIR)/,$(ETCFILES))

install: $(INSTALLTARGETS)

# Make target: make a .deb file in ../
BUILDPACKAGE = -us -uc --build=full -Iolle
deb:
	dpkg-buildpackage $(BUILDPACKAGE)
#	PREFIX= INCLUDE_PREFIX=/usr
