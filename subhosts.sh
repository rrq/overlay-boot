#!/usr/bin/env /lib/init/init-d-script
### BEGIN INIT INFO
# Provides:          subhosts
# Required-Start:    $syslog $time $remote_fs
# Required-Stop:     $syslog $time $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start subhosts
# Description:       Init script to start subhosts as per /etc/subhosts.conf
### END INIT INFO

# Note that this facility only acts on start/stop/status, and it does
# not have any running daemon of its own. The started subhosts will
# "run" separately.

DAEMON=none
NAME=subhosts

do_status_override() {
    pgrep -a overlay-boot | awk '{print $4}'
}

do_start_override() {
    if [ -r /etc/subhosts.conf ] ; then
	for SUB in $(grep -v '#' /etc/subhosts.conf) ; do
	    echo "overlay-boot $SUB" >&2
	    overlay-boot $SUB | logger -p boot.info -s
	done
    fi
}

do_stop_override() {
    for SUB in $(do_status_override) ; do
	echo "overlay-stop $SUB" >&2
	overlay-stop $SUB | logger -p boot.info -s
    done
}
